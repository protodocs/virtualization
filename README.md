[![pipeline status](https://gitlab.com/protodocs/virtualization/badges/main/pipeline.svg)](https://gitlab.com/protodocs/virtualization/-/commits/main)
[![Latest Release](https://gitlab.com/protodocs/virtualization/-/badges/release.svg)](https://gitlab.com/protodocs/virtualization/-/releases)

A website explaining my efforts on virtualization on Linux and macOS

Follow [this link](https://virtualization-protodocs-8333f534c4688bff1ab1fcff9e7317b62f50b5.gitlab.io/) to read the articles
