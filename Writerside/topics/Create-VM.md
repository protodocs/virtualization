# Create VM

Following chapter details how to create VM's in Proxmox

> Note that configuring DHCP networking should be configured
> with static IP leases in eg. the router if used
> in a Kubernetes cluster
{style="warning"}

## Create VM from template

> Note: do not forget to adjust disk size
{style="warning"}

1. Right-click on the appropriate template and select “Clone”
2. Select “Full clone” and specify the ID and name
3. Select VM and Tab "Hardware" and modify Memory and Cores settings accordingly
4. Adjust disk size accordingly
5. Start the new VM and wait till cloud-init has finished completely
6. Login with credentials from template
7. Run ```ip a``` to check network connectivity
8. Run ```sudo apt install -y qemu-guest-agent```
9. Reboot VM
10. Have fun!
