# Helm

![boat.svg](boat.svg)

In order to deploy Helm charts, install the Helm binaries.

Install instructions and downloads are available at the [Helm homepage](https://helm.sh)
One option is to download the latest release, for example:
```Bash
wget https://get.helm.sh/helm-v3.13.3-linux-amd64.tar.gz
tar xvzf helm-v3.13.3-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin
```