# Kubernetes

## Create a cluster

* From the VM template, clone at least one VM for a controller and one for a worker node
* Select mode “Full clone”
* Controller node should at least have 2 cores and 2G of memory
* Worker nodes should at least have 2 cores and 1G of memory (pref. 2G memory is better)
* Resize each VM disk size to eg. 20G for installing additional components
* Select "Start at boot" (optionally)
* For each node:
  1. Start the new VM and wait till cloud-init has finished completely
  2. Login with credentials from template
  3. Run “ip a” to check network connectivity
  4. Run “sudo apt install -y qemu-guest-agent”
  5. Reboot VM

> The IP address **must** become a static lease in the DHCP server.
> Mandatory for the Kubernetes cluster setup.
{style="warning"}


## Install container runtime (on each node)

```Bash
sudo apt install -y containerd
systemctl status containerd
sudo mkdir /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml
```
* Edit config.toml file and look for line “SystemdCgroup = false”
* Change setting to “true”

### Disable swap memory (if needed)
```Bash
swapoff -a
```
* Comment line in /etc/fstab

### Configure network and kernel modules

* Edit /etc/sysctl.conf
* Look for line “net.ipv4.ip_forward=1” and uncomment it
* (Optionally) do similar for ipv6
* Edit /etc/modules-load.d/k8s.conf
* Enter line “br_netfilter” and line "overlay"
```Bash
sudo modprobe br_netfilter
sudo modprobe overlay
sudo reboot
```

## Install Kubernetes binaries

> Kubernetes packages will be taken from **NEW** location
{style="note"}
```Bash
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
sudo apt update
sudo apt install -y kubeadm kubectl kubelet
sudo apt-mark hold kubelet kubeadm kubectl
``` 

## Specifics for the control plane nodes

To start a new control plane, execute below command:
```Bash
sudo kubeadm init —control-plane-endpoint=<ip-address of the VM> —pod-network-cidr=10.244.0.0/16
```

Note that the token expires. To create a new one, execute:
```Bash
kubeadm token create --print-join-command
```

> Additional control plane nodes require multiple certificate files.
> Copy following files to each control plane node from the originating node:
> * /etc/kubernetes/pki/ca.crt
> * /etc/kubernetes/pki/ca.key
> * /etc/kubernetes/pki/sa.key
> * /etc/kubernetes/pki/sa.pub
> * /etc/kubernetes/pki/front-proxy-ca.crt
> * /etc/kubernetes/pki/front-proxy-ca.key
> * /etc/kubernetes/pki/ca.crt
> * /etc/kubernetes/pki/etcd/ca.crt
> * /etc/kubernetes/pki/etcd/ca.key