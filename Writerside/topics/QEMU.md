# QEMU

Qemu is a virtualization solution for eg. Windows, MacOS and Linux
See the [QEMU homepage](https://qemu.org) for more details and downloads

The following paragraphs are based on version 8.1.3 of Qemu

![Qemu homepage](qemu_homepage.png){border-effect="rounded"}
