# RKE2

> Note: run below instructions with **root privileges**
{style="note"}

## Master nodes
> Create first master node with an (optional) token. If not
> specified, a token will be generated in `/var/lib/rancher/rke2/server/token`
> This token is needed for each subsequent node (master or worker) than joins
> the cluster.
> tls-san entries are needed if names are not fixed or listen to multiple domains.

```Bash
mkdir -p /etc/rancher/rke2/
vim /etc/rancher/rke2/config.yaml
```
Specify the below details and save file
```yaml
token: my-shared-secret
tls-san:
  - my-kubernetes-domain.com
  - another-kubernetes-domain.com
```
Execute following commands to download and install RKE2 binaries

```Bash
curl -sfL https://get.rke2.io | sh -
systemctl enable rke2-server.service
systemctl start rke2-server.service
```

> Note: all other master nodes require the following format in the
> `/etc/rancher/rke2/config.yaml` file, specifying the address of
> the first master node
> ```yaml
> token: my-shared-secret
> server: https://<DNS-DOMAIN>:9345
> tls-san:
>   - my-kubernetes-domain.com
>   - another-kubernetes-domain.com
> ```
{style="note"}

## Worker nodes

```Bash
curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE="agent" sh -
systemctl enable rke2-agent.service
mkdir -p /etc/rancher/rke2/
vim /etc/rancher/rke2/config.yaml
```