# RXJS

## Actions
The basis of RXJS, sending and receiving actions
Components dispatch actions

## Reducers
* Reducers listen/detects actions
* Reducers updates the store
* Reducers should act **immediately** (non-blocking)

## Store
The store is like a big database which contains the state of the app

## Selectors
Selectors select specific data from the store

## Effects
Effects may trigger services (eg. "long-running" API calls)