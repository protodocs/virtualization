# Rancher

Installation instruction can be found [here](https://ranchermanager.docs.rancher.com/pages-for-subheaders/install-upgrade-on-a-kubernetes-cluster)
> Important to download and install Helm and cert-manager first
{style="note"}

Add the Helm repo (stable is recommended) to deploy Rancher into the Kubernetes cluster
```Bash
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
```

Install cert-manager in case no private certificates are installed. 
Instead, Rancher or Let's Encrypt will generate the necessary certificates and store them using cert-manager.
At time of writing, version **v1.13.3** was the latest stable version.
```Bash
# If you have installed the CRDs manually instead of with the `--set installCRDs=true` option added to your Helm install command, you should upgrade your CRD resources before upgrading the Helm chart:
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.13.3/cert-manager.crds.yaml

# Add the Jetstack Helm repository
helm repo add jetstack https://charts.jetstack.io

# Update your local Helm chart repository cache
helm repo update

# Install the cert-manager Helm chart
helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace```