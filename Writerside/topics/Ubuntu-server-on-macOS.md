# Ubuntu server on macOS

## QEMU for MacOS

With QEMU it is possible to run eg. Linux as a virtual machine on your Mac. A few steps are needed to startup your first virtual machine.
Linux comes in many flavors, so-called distributions and below example takes Ubuntu server to create a VM.
The most obvious choice might be to take the Ubuntu live server ISO images, and run it to create and setup the VM.
But instead below describes how we configure an almost up-and-running image and use cloud-init to insert the necessary details.

### Install QEMU for MacOS
Install the QEMU binaries as described on the [macOS](https://www.qemu.org/download/#macos) page.
We took the method of Homebrew so running ```brew install qemu``` did the trick.
While we're at it, also make sure that ```brew install cdrtools``` has been executed.
Next step is to get an Ubuntu server image and prepare a VM environment.

### Download the cloud image

Navigate to the [Ubuntu cloud images](https://cloud-images.ubuntu.com) page and download the appropriate image.
In this example we use a Macbook Pro M1 that runs MacOS Sonoma and hence we download the image that is built wrt. the ARM-64 architecture.
Second, we'll go for the latest Ubuntu server LTS (22.04 aka Jammy Jellyfish) which can be pulled from [this link](https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-arm64.img) under directory 'current'.

> Check with the [SHA256SUMS](https://cloud-images.ubuntu.com/jammy/current/SHA256SUMS) file if the hash corresponds with your downloaded image

### Prepare for a VM

Create a separate directory for a VM and prepare the cloud-init files, for example:
```Bash
mkdir VM-001
cd VM-001
# Cloud-init files
touch meta-data
touch user-data
# VM startup script
touch startup.sh
# Copy the Ubuntu cloud image from your download location into this directory
cp ~/Downloads/jammy-server-cloudimg-arm64.img .
```

### Create the Cloud Init ISO image

Open the meta-data file and enter the following information.
Take any instance-id and local-hostname but note that they differ across VM's
```Text
instance-id: ubuntu2204-local
local-hostname: ubuntu2204
```

Open the user-data file and enter the following information.
This will enable the default user **`ubuntu`** with a password
and ability to logon directly from the terminal.
```Text
#cloud-config
password: <YOUR_OWN_SECRET>
chpasswd: { expire: false }
ssh_pwauth: true
```

Finally, create the ISO image by executing following command:
```Bash
mkisofs -output cidata.iso -volid cidata -joliet -rock user-data meta-data
```
The resulting **`cidata.iso`** will be used when starting up the VM and initializes the cloud image

### Create startup script
For convenience, create a small shell script to startup the VM.
Edit the file `startup.sh` and add the following command(s):
```Bash
qemu-system-aarch64  \
  -accel hvf \
  -m 2G \
  -cpu host \
  -smp 4 \
  -machine type=virt,highmem=off \
  -bios edk2-aarch64-code.fd \
  -device virtio-gpu-pci \
  -display default,show-cursor=on \
  -serial telnet::4444,server,nowait \
  -device virtio-blk-device,drive=hd0 \
  -drive if=none,file=jammy-server-cloudimg-arm64.img,format=qcow2,id=hd0 \
  -device virtio-net-device,netdev=net0 \
  -netdev user,id=net0 \
  -vga none -device ramfb \
  -cdrom cidata.iso \
  -device qemu-xhci -device usb-ehci -device usb-kbd -device usb-mouse -usb \
  -monitor stdio
```

This script will startup a VM in a separate window and initializes it with the cloud init data from the ISO image.
> This script will not run with a proper BIOS file. For this specific example, the bios can be downloaded
> from the QEMU [source repo](https://gitlab.com/qemu-project/qemu/-/tree/master/pc-bios?ref_type=heads).
> Choose for macOS on M1/2/3 architecture, the file named **`edk2-aarch64-code.fd.bz2`** and unzip with `bunzip2 edk2-aarch64-code.fd.bz2`
> Other architectures like x86 require different file or can even choose a different BIOS altogether.
{style="warning"}