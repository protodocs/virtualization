# Unifi

Steps to install the Unifi Server application on a Ubuntu based server (eg. 22.04)

1. Create a VM with a full clone from a Ubuntu cloud image template
2. Change hardware to have at least 8G of memory and 4 cores
3. First start, install the QEMU guest agent and reboot
4. Install MongoDB

> Ubuntu 22.04 and higher do not have the libssl1.1 dependency out of the box and
> need to be installed separately. Execute below lines to install before installing
> MongoDB
{style = "note"}

```Bash
wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb
sudo dpkg -i libssl1.1_1.1.0g-2ubuntu4_amd64.deb
```

> Unifi Server depends on an older version of MongoDB (v3.6)
> but verified to work with version 4.4. Note that eg. version 7
> requires the VM to run a newer type of CPU that might not be
> supported by the host CPU, otherwise the `mongod` service may
> throw `Core dump` errors due to unsupported code.
{style="warning"}

```Bash
sudo apt install -y gnupg curl ca-certificates apt-transport-https
curl -fsSL https://pgp.mongodb.com/server-4.4.asc | \
   sudo gpg -o /usr/share/keyrings/mongodb-server-4.4.gpg \
   --dearmor
echo "deb [ arch=amd64 signed-by=/usr/share/keyrings/mongodb-server-4.4.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt update
sudo apt install -y mongodb-org
```

> Optionally, the version of MongoDB can be frozen so upgrades are ignored.
> ```Bash
> echo "mongodb-org hold" | sudo dpkg --set-selections
> echo "mongodb-org-database hold" | sudo dpkg --set-selections
> echo "mongodb-org-server hold" | sudo dpkg --set-selections
> echo "mongodb-mongosh hold" | sudo dpkg --set-selections
> echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
> echo "mongodb-org-tools hold" | sudo dpkg --set-selections
> ```

5. Install Unifi

```Bash
echo 'deb [ arch=amd64,arm64 ] https://www.ui.com/downloads/unifi/debian stable ubiquiti' | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
sudo wget -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg
sudo apt update && sudo apt install unifi -y
```




