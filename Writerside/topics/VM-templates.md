# Create VM template

Following chapters describe how to create a VM template from Ubuntu cloud-images. This template is used to clone actual VM's from to create eg. Kubernetes cluster

## Create a VM
![create-VM-general.png](create-VM-general.png){style="inline" width="600"}
1. On the PVE (Proxmox-VE), create a new VM
2. Give it a high number on the ID, eg. 900
3. Give it a name eg. “ubuntu-2204-template”
4. On the OS tab, click ‘Do not use any media’
![create-VM-OS.png](create-VM-OS.png){style="inline" width="600"}
5. On System tab, check the ‘Qemu Agent’ box
![create-VM-system.png](create-VM-system.png){style="inline" width="600"}
6. On the Disks, remove the existing scsi disk
![create-VM-disks.png](create-VM-disks.png){style="inline" width="600"}
7. On the CPU tab, keep unchanged
![create-VM-CPU.png](create-VM-CPU.png){style="inline" width="600"}
8. On the Memory tab, keep unchanged
![create-VM-memory.png](create-VM-memory.png){style="inline" width="600"}
9. On the Network tab, keep unchanged
![create-VM-network.png](create-VM-network.png){style="inline" width="600"}
10. Confirm to create VM
![create-VM-confirm.png](create-VM-confirm.png){style="inline" width="600"}

## Change VM parameters

1. Select the new VM in the server view
2. Select the Hardware tab
3. Add a ‘Cloudinit Drive’
![create-VM-HW-add-cloudinit.png](create-VM-HW-add-cloudinit.png){style="inline" width="500"}
4. Select 'IDE-0' as a device and ‘local-lvm’ as the storage option and finish
5. Select the Cloud-Init tab
6. Specify the “User” and “Password” details
7. Specify the SSH keys
> SSH keys are mandatory if remote logon needs to work.
> Can also be changed when cloning from template
{style="warning"}
8. Select IP Config and select “DHCP” on both IPV4 and IPV6
![create-VM-HW-overview.png](create-VM-HW-overview.png){style="inline" width="400"}
9. Click “Regenerate Image” in the menu

## Attach cloud image

1. SSH into the Proxmox server
2. Decide which cloud image to use, eg. Ubuntu Jammy (22.04)
```bash
wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
qm set <VMID> -—serial0 socket -—vga serial0
# (IMPORTANT: Check if ‘local-lvm’ is the local storage on proxmox server)
qm disk import <VMID> /root/jammy-server-cloudimg-amd64.img local-lvm
```

1. Switch back to GUI
2. Select the VM and tab “Hardware”
3. Double-click the line “Unused Disk 0”
4. If underlying disk is SSD, check the 'SSD emulation' and ‘Discard’ option
![create-VM-disks-associate.png](create-VM-disks-associate.png){style="inline" width="500"}
5. Click "Add"
6. Select the tab “Options” and the line “Boot order”
7. Check the newly added disk and drag it to e.g. the second position
![create-VM-HW-bootorder.png](create-VM-HW-bootorder.png){style="inline" width="400"}
8. Check the ‘Start at Boot’ option (optionally)
9. Right-click on the VM and select “Convert to template”

