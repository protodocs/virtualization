# Virtual datacenter

Following chapters describe how to create and configure a (virtual) datacenter through [Proxmox](https://proxmox.com)
* Download the ISO image from [here](https://www.proxmox.com/en/downloads)
* Write the ISO image to a USB stick, for example (on Linux):
```
dd if=/proxmox-ve_8.1-1.iso of=/dev/disk<x> bs=1m
```
* Boot from USB stick on a server, for example a Intel NUC13 with i7/i9, 32Gb and 2Tb SSD
* Following the instructions:
  * Install on EXT4 filesystem (ZFS only if more disks AND more memory are installed and the redundancy is a must)
  * Accept defaults on filesystem
  * Select (default) country / timezone
  * Specify root password and email
  * Name the server(s) like pve-01, pve-02, etc
  * Remove USB and reboot
* Login to http://&lt;ip-address-of-proxmox-server>:8006
* Navigate to eg. pve-01 (depends on name when installing) and select "Updates -> Repositories"
* De-select 'Enterprise repository' entries and add 'No-subscription' entries
* Navigate to "Updates" and click "Refresh". Upgrade accordingly and reboot PVE

> Reboot to make sure upgrades take effect!
>
{style="note"}

Optionally, to avoid the QM command complaining about locale unset from eg. MacOS terminal
Create a .bash_profile and add following lines:
```Bash
export LANG="en_US.utf8"
export LC_ALL=C
export LANGUAGE=en_US
```
