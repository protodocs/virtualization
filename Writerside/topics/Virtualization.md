# Virtualization

Virtualization is a technique to run one or more so-called virtual machines simultaneously on a single physical machine.

This website describes two practical examples. One is based on QEMU and works on many platforms like Windows, macOS and Linux.
Second is Proxmox, a virtualization solution which runs on Linux and able to manage multiple VM's and containers from a nice GUI.

Each subtopic details the steps needed to get up-and-running and add additional features.
